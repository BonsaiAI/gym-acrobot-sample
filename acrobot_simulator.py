import gym

import bonsai
from bonsai_gym_common import GymSimulator

ENVIRONMENT = 'Acrobot-v1'
RECORD_PATH = None


class AcrobotSimulator(GymSimulator):

    def __init__(self, env, record_path, render_env):
        GymSimulator.__init__(
            self, env, skip_frame=1,
            record_path=record_path, render_env=render_env)

    def get_state(self):
        parent_state = GymSimulator.get_state(self)
        state_dict = {"cos_theta0": parent_state.state[0],
                      "sin_theta0": parent_state.state[1],
                      "cos_theta1": parent_state.state[2],
                      "sin_theta1": parent_state.state[3],
                      "theta0_dot": parent_state.state[4],
                      "theta1_dot": parent_state.state[5]}
        return bonsai.simulator.SimState(state_dict, parent_state.is_terminal)


if __name__ == "__main__":
    env = gym.make(ENVIRONMENT)
    base_args = bonsai.parse_base_arguments()
    simulator = AcrobotSimulator(env, RECORD_PATH, not base_args.headless)
    bonsai.run_with_url("acrobot_simulator", simulator, base_args.brain_url)
